import { createContext, useState } from "react";
import HomePage from "./pages";
import { getStorage } from "./util";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { CartPage } from "./pages/CartPage";
import { FavoritePage } from "./pages/FavoritePage";
import Layout from "./pages/Layout";

export const Context = createContext();

function App() {
	const favoriteIds = getStorage("favorite");
	const cart = getStorage("cart");

	const [state, setState] = useState({ favoriteIds, cart });

	return (
		<Context.Provider value={[state, setState]}>
			<BrowserRouter>
				<Routes>
					<Route path="/" element={<Layout />}>
						<Route index element={<HomePage />} />
						<Route path={`/cart`} element={<CartPage />} />
						<Route path={`/favorites`} element={<FavoritePage />} />
					</Route>
				</Routes>
			</BrowserRouter>
		</Context.Provider>
	);
}

export default App;
