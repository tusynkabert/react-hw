import React from "react";
import ProductList from "../components/ProductList";
import Nav from "../components/Nav";

const HomePage = () => {
    return <>
        <ProductList />
    </>
}

export default HomePage;