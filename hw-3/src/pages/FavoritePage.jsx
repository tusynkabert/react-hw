import React, { useContext } from "react";
import Nav from "../components/Nav";
import { Context } from "../App";
import ProductCard from "../components/ProductCard";
import { getStorage } from "../util";

export const FavoritePage = () => {

    const [{favoriteIds, cart}] = useContext(Context);

    const products = getStorage('products')

    const selected = products.filter(p => favoriteIds.includes(p.id))

    return <div className="container pt-[140px] px-10 sm:px-8 mx-auto mb-14 flex flex-wrap gap-6 justify-between">
                {
                    selected.length === 0
                    ? <h2>No favorite products!</h2>
                    : selected.map((product) => (
                        <ProductCard key={product.id} product={product} />
                    ))}
            </div>
}
 