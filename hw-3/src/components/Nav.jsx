import Logo from "./NavComponents/Logo";
import Menu from "./NavComponents/Menu";
import Cart from "./NavComponents/Cart";
import Favorite from "./NavComponents/Favorite";

export default function Nav() {
	return (
		<div className="w-full bg-cyan-50 border-b-[1px] fixed z-10">
			<div className="container sm:px-8 mx-auto px-10 py-6 flex justify-between items-center">
				<Logo />
				<div className="flex items-center divide-gray-400 divide-x-[1px]">
					<Menu />
					<div className="flex items-center gap-8 pl-6">
						<Favorite />
						<Cart />
					</div>
				</div>
			</div>
		</div>
	);
}
