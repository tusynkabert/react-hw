import React from "react";
import { Link } from "react-router-dom";

const Menu = () => {
	return (
		<>
			<div className="navigation__left navigation__links js-mobile-menu mr-2">
				<ul className="menu navigation__item-list">
					<li className="menu__li navigation__item">
						<Link to={"/"} className="menu__link link-hover">
							Головна
						</Link>
					</li>
					<li className="menu__li navigation__item">
						<Link to={"/cart"} className="menu__link link-hover">
							Cart
						</Link>
					</li>
					<li className="menu__li navigation__item">
						<Link to={"/favorites"} className="menu__link link-hover">
							Favorites
						</Link>
					</li>
				</ul>
			</div>
		</>
	);
};

export default Menu;
