import React from "react";
import PropTypes from 'prop-types'

const Button = ({ text, onClick, className, testId = '' }) => {
	return (
		<button data-testid={testId} className={className} onClick={onClick}>
			{text}
		</button>
	);
};

export default Button;

Button.defaultProps = {
	className: ''
}

Button.propTypes = {
	text: PropTypes.string,
	onClick: PropTypes.func.isRequired,
	className: PropTypes?.string
}