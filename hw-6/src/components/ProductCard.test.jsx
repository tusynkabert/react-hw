import { screen, render, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import ProductCard from "./ProductCard";

 // Отримуємо функцію для моку стору
const mockStore = configureStore([]);

describe("Product card", () => {

	test("render card", async () => {

         // Мокаємо Редакс стор
		const store = mockStore({
			favorites: {
				list: [],
			},
			cart: {
				list: [],
			},
		});

         // Створюємо мокані (несправжні чи тестові) дані товару
		const mockProduct = {
			id: 1,
			name: "iPhone",
			price: 500,
			img: "",
			article: "333",
			color: "red",
		};

         // Відмальовуємо компонент
		const productCart = render(
			<Provider store={store}>
				<ProductCard product={mockProduct} />
			</Provider>
		);

         // Перевіряємо чи карточка була відмальована (чи є заголовок)
        expect(screen.getByText(/iphone/i)).toBeInTheDocument();

       // Вибираємо кнопку "Додати в корзину"
		const button = screen.getByRole("button", {
			name: /add to cart/i,
		});

         // Клікаємо на кнопку "Додати в корзину"
		fireEvent.click(button);

         // Вибираємо кнопку "Додати в козину" з модального вікна
        const modalAddToCard = screen.getByTestId('modal-add-to-card')

         // Перевіряємо чи є кнопка "Скасувати"
		expect(screen.getByText(/cancel/i)).toBeInTheDocument();

         // Клакаємо на кнопку в модальному вікні і додаємо товар в корзину
        fireEvent.click(modalAddToCard)

         // Перевіряємо чи товар додався в корзину і кнопка змінила текст
		expect(screen.getByText(/added to cart/i)).toBeInTheDocument();

         // Створюємо snapshot
        expect(productCart).toMatchSnapshot()

         // Дебагінг
		// screen.logTestingPlaygroundURL();
	});
});
