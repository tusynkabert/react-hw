import PropTypes from "prop-types";
import { useState } from "react";
import Modal from "./Modal";

export default function CartItem({ item, delCartProduct }) {

	// Стани для відстеження стану модального вікна та визначення, чи продукт є у списку улюблених
	const [open, setOpen] = useState(false);

	return (
		<>
		<li  className="cursor-pointer px-2 py-4 flex items-center justify-between hover:bg-gray-100">
			<div className="flex items-center gap-2 max-w-[50%]">
				<div className="h-10 w-10 min-w-[40px] rounded-lg overflow-hidden object-cover">
					<img className="m-0 p-0 w-full h-full object-cover" src={item.img} alt="" />
				</div>
				<div>
					<p>{item.name}</p>
					<div className="h-4 w-4" style={{ background: item.color }}></div>
				</div>
			</div>
			<div className="grid grid-cols-2 ml-auto">
				<span className="text-gray-500 min-w-[50px]">x{item.quantity}</span>
				<span className="min-w-[150px]">{(+item.price * +item.quantity).toFixed(2)}</span>
			</div>
			<div className="flex items-center justify-center min-w-[50px] p-2" onClick={() => setOpen(true)}>
				x
			</div>
		</li>
		{open ? <Modal setOpen={setOpen} open={open} eventModal={() => { delCartProduct(item.id); setOpen(false)}} title="Delete from cart?" buttonEvent="Delete" /> : ""}
		</>
	);
}

CartItem.propTypes = {
	item: PropTypes.shape({
		name: PropTypes.string,
		price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
		quantity: PropTypes.number,
		img: PropTypes.string,
		color: PropTypes.string
	})
};