import React, { useContext }  from "react";
import ProductCard from "./ProductCard";
import { useSelector } from "react-redux";
import { CardsContext } from "../App";

const ProductList = () => {
	const productsRedux = useSelector((state ) => state.products.products)
	
	// Витягуємо з контексту масив корзини
	const cart = useSelector((state) => state.cart.list)
// стан вигляду карточок (картки чи таблиця)
	const { cardsView, setCardsView } = useContext(CardsContext); 

	return (
		<div className="pt-[140px] container px-10 sm:px-8 mx-auto ">
			
			{/* задаємо вид карточкам (on click) */}
			<div className="flex justify-end pb-10">			
				<button onClick={() => setCardsView(false)} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 mr-1 rounded focus:outline-none focus:shadow-outline-blue" id="grid-view-btn">
					Картки
				</button>
				<button onClick={() => setCardsView(true)} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline-blue" id="table-view-btn">
					Таблиця
				</button>
			</div>
			
			{/* відмальовує вид карточками або таблицею */}
			<div className={`mb-14 flex flex-wrap gap-6 justify-between ${(cardsView) ? 'view-cards-table' : ''}`} >
				{productsRedux.map((product) => (
					<ProductCard key={product.id} product={product} isExist={cart.some((item) => item.id === product.id)} />
				))}
			</div>
		</div>
	);
};

export default ProductList;
