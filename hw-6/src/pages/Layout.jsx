import { Outlet } from "react-router-dom";
import Nav from "../components/Nav";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { fetchProducts } from "../redux/productsSlice.js";

const Layout = () => {
	const dispatch = useDispatch();
	// Отримуємо товари
	useEffect(() => {
		dispatch(fetchProducts());
	}, [dispatch]);

	return (
		<>
			<Nav />
			<Outlet />
		</>
	);
};

export default Layout;
