import React from "react";
import { useSelector } from "react-redux";
import CartEmpty from "../components/CartComponents/CartEmpty";
import CartFilled from "../components/CartComponents/CartFilled";

export const CartPage = () => {

    const cart = useSelector((state) => state.cart.list)

    return <div className="container pt-[140px] px-10 sm:px-8 mx-auto mb-14">
                {
                cart.length === 0
                    ? <CartEmpty />
                    : <CartFilled cart={cart} />
                }
            </div>
}
 