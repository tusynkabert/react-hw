import { Link } from "react-router-dom";

export default function ThankYou() {

  return (
    <div className="container pt-[140px] px-10 sm:px-8 mx-auto mb-14 text-center">
        <img src="img/success.png" alt="Success" width="512" height="512" className="max-w-[300px] inline-block"/>
        <h1 className="text-8xl font-bold text-[#43d685] py-6">Thank You!</h1>
        <p className="text-lg text-gray-500 pb-8">Thank you for your interest! <br />Our manager will contact you to clarify the order.</p>
        <Link to="/" className="rounded-md bg-indigo-600 px-12 py-5 text-lg font-semibold text-white shadow-sm hover:bg-indigo-500 hover:text-white focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
            Main page
        </Link>
    </div>
  )
}
