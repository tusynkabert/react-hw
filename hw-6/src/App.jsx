import { createContext, useState } from "react";
import HomePage from "./pages";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { CartPage } from "./pages/CartPage";
import { FavoritePage } from "./pages/FavoritePage";
import Layout from "./pages/Layout";
import { Provider } from "react-redux";
import { store } from "./store.js";
import ThankYou from "./pages/ThankYou";


const CardsContext = createContext()

const NotFound = () => (
	<h1 className="pt-40">NOT FOUND</h1>
)

function App() {

	const [cardsView, setCardsView] = useState(false);

	return (
		<CardsContext.Provider value={{cardsView, setCardsView}}>
			<Provider store={store}>
				<BrowserRouter>
					<Routes>
						<Route path="/" element={<Layout />}>
							<Route index element={<HomePage />} />
							<Route path={`/cart`} element={<CartPage />} />
							<Route path={`/favorites`} element={<FavoritePage />} />
							<Route path={`/thank-you`} element={<ThankYou />} />
							{/* <Route path={"/*"} element={<NotFound />} /> */}
						</Route>
					</Routes>
				</BrowserRouter>
			</Provider>
		</CardsContext.Provider>
	);
}

export default App;
export { CardsContext };
