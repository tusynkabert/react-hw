import favoritesReducer, {toggleFavorites} from './favoritesSlice';

test('should add if not in favorites', () => { 

    const initialState = {
        list: []
    }

    const action = toggleFavorites(1);

    const newState = favoritesReducer(initialState, action);

    const expectedValue = {
        list: [1]
    }

    expect(newState).toEqual(expectedValue)

 })


 test('should remove if already in favorites', () => { 

    const initialState = {
        list: [1]
    }

    const action = toggleFavorites(1);

    const newState = favoritesReducer(initialState, action);

    const expectedValue = {
        list: []
    }

    expect(newState).toEqual(expectedValue)
    
  })