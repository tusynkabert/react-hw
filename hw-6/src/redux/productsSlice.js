import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios';


export const fetchProducts = createAsyncThunk(
  'products/fetchProducts',
  async function() {
    // Запит до сервера для отримання списку товарів
    const res = await axios.get("/products.json");
    // await new Promise((resolve, rej) => {
    //   setTimeout(() => {
    //     return resolve()
    //   }, 2000 )
    // })
    // Отримання та збереження списку товарів
    const products = res.data.products;

    // Повертаємо список товарів
    return products;
  }
);

const productsSlice = createSlice({
  name: 'products',
  initialState: {
    products: [],
    loading: true
  },
  reducers: {
    addProducts(state, action) {
      state.products.push(...action.payload)
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.products = action.payload;
        state.loading = false
      })
  },
})

export const { addProducts } = productsSlice.actions;
export default productsSlice.reducer;