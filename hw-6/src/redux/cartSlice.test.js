import cartReducer, { addToCart, removeProductFromCart, cleanCart } from './cartSlice';

describe("Cart Slice", () => {

    test("Add to cart", () => {


        // initial state
        const initialState = {
            list: []
        }

        // products
        const _IPHONE_ = {
            id: 1,
            name: 'test'
        }
        const _NOTEBOOK_ = {
            id: 2,
            name: 'product 2'
        }

        // expected values
        const expectedValueAfterAdd_IPHONE_ = {
            list: [
                {
                    id: 1,
                    name: 'test',
                    quantity: 1
                }
            ]
        }
        const expectedValueAfterAdd_IPHONE_Again = {
            list: [
                {
                    id: 1,
                    name: 'test',
                    quantity: 2
                }
            ]
        }
        const expectedValueWithTwo_IPHONE_And_NOTEBOOK_ = {
            list: [
                {
                    id: 1,
                    name: 'test',
                    quantity: 2
                },
                {
                    id: 2,
                    name: 'product 2', quantity: 1
                }
            ]
        }

        // actions
        const action_IPHONE_ = addToCart(_IPHONE_);
        const action_NOTEBOOK_ = addToCart(_NOTEBOOK_);

        // test 1
        const stateWith_IPHONE_ = cartReducer(initialState, action_IPHONE_)
        expect(stateWith_IPHONE_).toEqual(expectedValueAfterAdd_IPHONE_)

        // test 2
        const stateWithTwo_IPHONE_ = cartReducer(stateWith_IPHONE_, action_IPHONE_)
        expect(stateWithTwo_IPHONE_).toEqual(expectedValueAfterAdd_IPHONE_Again)

        // test 3
        const stateWithTwo_IPHONE_And_NOTEBOOK_ = cartReducer(stateWithTwo_IPHONE_, action_NOTEBOOK_)
        expect(stateWithTwo_IPHONE_And_NOTEBOOK_).toEqual(expectedValueWithTwo_IPHONE_And_NOTEBOOK_)

    })

    test("Remove product from cart", () => {


        const initialState = {
            list: [
                {
                    id: 1,
                    name: 'test'
                },
                {
                    id: 2,
                    name: 'product 2'
                }
            ]
        }

        const action = removeProductFromCart(1)

        const newState = cartReducer(initialState, action)

        const expectedValue = {
            list: [
                {
                    id: 2,
                    name: 'product 2'
                }
            ]
        }

        expect(newState).toEqual(expectedValue)


    })

    test('Clean cart', () => {

        const initialState = {
            list: [
                {
                    id: 1,
                    name: 'test'
                },
                {
                    id: 2,
                    name: 'product 2'
                }
            ]
        }

        const action = cleanCart()

        const newState = cartReducer(initialState, action)

        const expectedValue = {
            list: []
        }

        expect(newState).toEqual(expectedValue)
    })

})