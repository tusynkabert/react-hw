import { createSlice } from '@reduxjs/toolkit'
import { getStorage, setStorage } from '../util';

const cartSlice = createSlice({
    name: 'cart',
    initialState: {
        list: getStorage('cart') || []
    },
    reducers: {
        addToCart(state, action) {
            const product = action.payload;
            const isAddedToCart = !!state.list.find((p) => p.id === product.id);

            if (isAddedToCart) {
                state.list = state.list.map((prod) => {
                    if (prod.id === product.id) {
                        prod.quantity = (prod.quantity || 0) + 1
                    }

                    return prod
                })

                return;
            }

            state.list.push({...product, quantity:1})
        },
        removeProductFromCart(state, action) {
            state.list = state.list.filter((li) => li.id !== action.payload)
            setStorage('cart', state.list);
        },
        cleanCart(state) {
            setStorage('cart', []);
            state.list = [];
        }
    },
})

export const { addToCart, removeProductFromCart, cleanCart } = cartSlice.actions;
export default cartSlice.reducer;
