import productReducer, {addProducts, fetchProducts} from './productsSlice.js';


describe('Product Slice', () => {

    test('fetch products', () => {

        // початковий стейт
        const initialState = {
            products: [],
            loading: true
          }
        
          // продукти, які повертає сервер
          const serverProductsResponse = [
            {
                id: 1,
                 name: 'iPhone'
            }
          ]

          // створюємо подію
        const action = fetchProducts.fulfilled(serverProductsResponse)

        // передаємо початковий стан і подію в редюсер і оримуємо новий стейт
        const newState = productReducer(initialState, action)

        // очікуваний стейт
        const expectedValue = {
            products: [{
                id:1,
                name: 'iPhone'
            }],
            loading: false
          }

          // тестуємо
        expect(newState).toEqual(expectedValue)

    })

    test('Add product', () => {

        // початковий стейт
        const initialState = {
            products: [],
            loading: true
          }
        
          // продукт
          const products = [
            {
                id: 1,
                 name: 'iPhone'
            }
          ]

          // створюємо подію
        const action = addProducts(products)

        const newState = productReducer(initialState, action)

        const expectedValue = {
            products: [
                {
                    id: 1,
                     name: 'iPhone'
                }
            ],
            loading: true
          }

        expect(newState).toEqual(expectedValue)


    })
})