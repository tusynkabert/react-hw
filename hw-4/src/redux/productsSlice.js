import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios';


export const fetchProducts = createAsyncThunk(
  'products/fetchProducts',
  async function() {
    // Запит до сервера для отримання списку товарів
    const res = await axios.get("/products.json");

    // Отримання та збереження списку товарів
    const products = res.data.products;

    // Повертаємо список товарів
    return products;
  }
);

const productsSlice = createSlice({
  name: 'products',
  initialState: {
    products: [],
  },
  reducers: {
    addProducts(state, action) {
      state.push(...action.payload)
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProducts.fulfilled, (state, action) => {
        state.products = action.payload;
      })
  },
})

export const { addProducts } = productsSlice.actions;
export default productsSlice.reducer;