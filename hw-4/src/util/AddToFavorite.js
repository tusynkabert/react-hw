import { getStorage, setStorage } from ".";

// Функція addToFavorite для додавання або видалення продукту зі списку улюблених
const addToFavorite = (id) => {
    // Отримання поточного списку улюблених з локального сховища
    const favoriteList = getStorage('favorite');

    // Пошук індексу продукту в списку улюблених
    const itemIndex = favoriteList.findIndex((existId) => existId === id);

    // Додавання або видалення продукту зі списку улюблених в залежності від його наявності
    if (itemIndex === -1)
        favoriteList.push(id); // Додаємо, якщо продукту ще немає в улюблених
    else
        favoriteList.splice(itemIndex, 1); // Видаляємо, якщо продукт вже був у списку

    // Оновлення списку улюблених в локальному сховищі
    setStorage('favorite', favoriteList);
}

export default addToFavorite;