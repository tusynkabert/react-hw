import React, { useContext, useEffect } from "react";
import ProductCard from "./ProductCard";

import { Context } from "../App";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from "../redux/productsSlice";

const ProductList = () => {
	const dispatch = useDispatch();
	const productsRedux = useSelector((state ) => state.products.products)

	// Витягуємо з контексту масив корзини
	const [{ cart }] = useContext(Context);

	// Отримуємо товари
	useEffect(() => {
		dispatch(fetchProducts());
	}, [dispatch])
	

	return (
		<>
			<div className="container pt-[140px] px-10 sm:px-8 mx-auto mb-14 flex flex-wrap gap-6 justify-between">
				{productsRedux.map((product) => (
					<ProductCard key={product.id} product={product} isExist={cart.some((item) => item.id === product.id)} />
				))}
			</div>
		</>
	);
};

export default ProductList;
