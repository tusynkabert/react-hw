import Button from "./Button";
import PropTypes from "prop-types";

const Modal = ({ setOpen, open, eventModal, title = 'Add to cart?', buttonEvent = 'Add to cart' }) => {
	return (
		<div className={`modal ${open ? 'show' : '' }`}>
			<div  className="modal__overlay"></div>
			<div className="modal__body">
				<Button onClick={() => setOpen(false)} className={"modal__close"} text="X" />
				<h2 className="modal__header">{title}</h2>
				<div className="modal__actions">
					<Button onClick={eventModal} text={buttonEvent} className="btn btn-ok" />
					<Button onClick={() => setOpen(false)} text={"Cancel"} className="btn btn-cancel" />
				</div>
			</div>
		</div>
	);
};

export default Modal;

Modal.propTypes = {
	setOpen: PropTypes.func,
	open: PropTypes.bool,
	eventAddToCart: PropTypes.func
}