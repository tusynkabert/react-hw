import React, { useContext, useEffect, useState } from "react";

import { getStorage } from "../../util";
import { Popover, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { Context } from "../../App";
import Button from "../Button";
import { classNames } from "../../util/classNames";
import { useSelector } from "react-redux";

const Favorite = () => {
	// Витягуємо з контексту масив улюблених товарів
	const [{ favoriteIds }, setContext] = useContext(Context);

	// Отримуємо товари з localstorage
	const products = useSelector(state => state.products.products);

	// Стан для зберігання улюблених продуктів
	const [favorite, setFavorite] = useState([]);

	// Функція для додавання продукту до корзини
	const addToCart = (id) => {
		favoriteIds.push(id);

		// Оновлення контексту з новим станом корзини
		setContext((prev) => ({
			...prev,
			favoriteIds,
		}));
	};

	const eventAddToCart = (product, id) => {
		addToCart(product,id);
	};

	// Записуємо товари в стан улюблених товарів
	const setFavoriteProducts = () => {

		// Фільтруємо продукти за включеними ідентифікаторами у favoriteIds
		const favotiteProducts = products.filter((p) => favoriteIds.includes(p.id));
		
		// Записуємо товари в стан улюблених товарів
		setFavorite(favotiteProducts);
	}
	
	// Ефект для оновлення улюблених продуктів при зміні favoriteIds
	useEffect(() => {
		
		// Записуємо товари в стан улюблених товарів
		setFavoriteProducts();

	// Вказуємо залежності
	}, [favoriteIds, products]);

	return (
		<div className="">
			<Popover className="relative">
				{({ open }) => (
					<>
						<Popover.Button className={` ${open} text-black group relative inline-flex items-center cursor-pointer hover:text-blue-700 rounded-md text-base font-medium  focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75`}>
							<div className="absolute z-50 bg-red-600 rounded-full h-6 min-w-[24px] flex items-center justify-center text-white -top-[20px] -right-[20px] text-sm">{favorite.length}</div>
							<svg className="icon icon-heart text-[20px]">
								<use href="#icon-heart"></use>
							</svg>
						</Popover.Button>
						<Transition as={Fragment} enter="transition ease-out duration-200" enterFrom="opacity-0 translate-y-1" enterTo="opacity-100 translate-y-0" leave="transition ease-in duration-150" leaveFrom="opacity-100 translate-y-0" leaveTo="opacity-0 translate-y-1">
							<Popover.Panel className="absolute right-0 z-10 mt-3 w-screen max-w-sm transform px-4 sm:px-0 lg:max-w-3xl">
								<div className="overflow-hidden rounded-lg shadow-lg ring-1 ring-black/5">
									<div className="bg-gray-50 p-4 flex flex-col gap-4">
										<ul className="flex flex-col gap-1">
											{
												favorite.length === 0
													? <h4>No results!</h4>
													: favorite.map((c, idx) => (
														<FavoriteItem key={idx} favItem={c} eventAddToCart={eventAddToCart} />
													))
											}
										</ul>
									</div>
									
								</div>
							</Popover.Panel>
						</Transition>
					</>
				)}
			</Popover>
		</div>
	);
};

function FavoriteItem ({favItem, eventAddToCart}) {

	const [{cart} ] = useContext(Context)

	const [isOnCart, setIsOnCart] = useState(false)

	useEffect(() => {
		setIsOnCart(cart.find(ci => ci.id === favItem.id))
	}, [cart])

	return <li  className=" cursor-pointer px-2 py-4 flex items-center justify-between hover:bg-gray-100">
	<div className="flex justify-between items-center gap-2 max-w-[50%]">
		<div className="h-10 w-10 min-w-[40px] rounded-lg overflow-hidden object-cover">
			<img className="m-0 p-0 w-full h-full object-cover" src={favItem.img} alt="" />
		</div>
		<div>
			<p>{favItem.name}</p>
			<div className="h-4 w-4" style={{ background: favItem.color }}></div>
		</div>
	</div>
		<Button className={classNames('p-4 cursor-pointer text-white rounded-md', isOnCart ? "bg-green-400" : "bg-blue-400")} text={isOnCart ? "Added to cart" :'Add to cart'} onClick={() => eventAddToCart(favItem.id)} />
</li>
}

export default Favorite;
