import React, { useContext } from "react";
import Nav from "../components/Nav";
import { Context } from "../App";
import ProductCard from "../components/ProductCard";

export const CartPage = () => {

    const [{cart}] = useContext(Context)

    return <div className="container pt-[140px] px-10 sm:px-8 mx-auto mb-14 flex flex-wrap gap-6 justify-between">
                {
                cart.length === 0
                    ? <h2>Cart empty!</h2>
                    : cart.map((product) => (
                    <ProductCard key={product.id} product={product} needButtonCart={false} />
                ))}
            </div>
}
 