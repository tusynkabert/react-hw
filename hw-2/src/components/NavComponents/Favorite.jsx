import React, { useContext, useEffect, useState } from "react";

import { getStorage } from "../../util";
import { Popover, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { Context } from "../../App";

const Favorite = () => {
	// Витягуємо з контексту масив улюблених товарів
	const [{ favoriteIds }] = useContext(Context);

	// Отримуємо товари з localstorage
	const products = getStorage("products");

	// Стан для зберігання улюблених продуктів
	const [favorite, setFavorite] = useState([]);

	// Ефект для оновлення улюблених продуктів при зміні favoriteIds
	useEffect(() => {
		// Фільтруємо продукти за включеними ідентифікаторами у favoriteIds
		setFavorite(products.filter((p) => favoriteIds.includes(p.id)));

	// Вказуємо залежності
	}, [favoriteIds]);

	return (
		<div className="">
			<Popover className="relative">
				{({ open }) => (
					<>
						<Popover.Button className={` ${open} text-black group relative inline-flex items-center cursor-pointer hover:text-blue-700 rounded-md text-base font-medium  focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75`}>
							<div className="absolute z-50 bg-red-600 rounded-full h-6 min-w-[24px] flex items-center justify-center text-white -top-[20px] -right-[20px] text-sm">{favorite.length}</div>
							<svg className="icon icon-heart text-[20px]">
								<use href="#icon-heart"></use>
							</svg>
						</Popover.Button>
						<Transition as={Fragment} enter="transition ease-out duration-200" enterFrom="opacity-0 translate-y-1" enterTo="opacity-100 translate-y-0" leave="transition ease-in duration-150" leaveFrom="opacity-100 translate-y-0" leaveTo="opacity-0 translate-y-1">
							<Popover.Panel className="absolute right-0 z-10 mt-3 w-screen max-w-sm transform px-4 sm:px-0 lg:max-w-3xl">
								<div className="overflow-hidden rounded-lg shadow-lg ring-1 ring-black/5">
									<div className="bg-gray-50 p-4">
										<ul className="flex flex-col gap-1">
											{favorite.map((c, idx) => (
												<li key={idx} className=" cursor-pointer px-2 py-4 flex items-center justify-between hover:bg-gray-100">
													<div className="flex items-center gap-2 max-w-[50%]">
														<div className="h-10 w-10 min-w-[40px] rounded-lg overflow-hidden object-cover">
															<img className="m-0 p-0 w-full h-full object-cover" src={c.img} alt="" />
														</div>
														<div>
															<p>{c.name}</p>
															<div className="h-4 w-4" style={{ background: c.color }}></div>
														</div>
													</div>
												</li>
											))}
										</ul>
									</div>
								</div>
							</Popover.Panel>
						</Transition>
					</>
				)}
			</Popover>
		</div>
	);
};

export default Favorite;
