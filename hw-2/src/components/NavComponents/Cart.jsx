import React, { useContext } from "react";

import { Popover, Transition } from "@headlessui/react";
import { Fragment } from "react";
import { Context } from "../../App";
import CartItem from "../CartItem";

const Cart = () => {

    // Витягуємо з контексту масив корзини
	const [{ cart }] = useContext(Context);

	return (
		<div className="">
			<Popover className="relative">
				{({ open }) => (
					<>
						<Popover.Button className={`${open} text-black group relative inline-flex items-center cursor-pointer hover:text-blue-700 rounded-md text-base font-medium  focus:outline-none focus-visible:ring-2 focus-visible:ring-white/75`}>
							<div className="absolute bg-red-600 rounded-full h-6 min-w-[24px] flex items-center justify-center text-white -top-[20px] -right-[20px] text-sm">{cart.length}</div>
							<svg className="icon icon-cart-bag text-[20px]">
								<use href="#icon-cart-bag"></use>
							</svg>
						</Popover.Button>
						<Transition as={Fragment} enter="transition ease-out duration-200" enterFrom="opacity-0 translate-y-1" enterTo="opacity-100 translate-y-0" leave="transition ease-in duration-150" leaveFrom="opacity-100 translate-y-0" leaveTo="opacity-0 translate-y-1">
							<Popover.Panel className="absolute right-0 z-10 mt-3 w-screen max-w-sm transform px-4 sm:px-0 lg:max-w-3xl">
								<div className="overflow-hidden rounded-lg shadow-lg ring-1 ring-black/5">
									<div className="bg-gray-50 p-4">
										<ul className="flex flex-col gap-1">
											{cart.map((c, idx) => (
												<CartItem key={idx} item={c} />
											))}
										</ul>
									</div>
								</div>
							</Popover.Panel>
						</Transition>
					</>
				)}
			</Popover>
		</div>
	);
};

export default Cart;
