import React from "react";

const Logo = () => {
	return (
		<>
			<div className="navigation__middle">
				<a href="index.html" className="text-2xl flex items-center">
					<img src="img/logo.png" className="logo-img max-w-[52px] mr-2" alt="react shop" />
					React Shop
				</a>
			</div>
		</>
	);
};

export default Logo;
