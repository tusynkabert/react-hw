import React from "react";

const Menu = () => {
	return (
		<>
			<div className="navigation__left navigation__links js-mobile-menu mr-2">
				<ul className="menu navigation__item-list">
					<li className="menu__li navigation__item">
						<a href="index.html" className="menu__link link-hover">
							Головна
						</a>
					</li>
					<li className="menu__li navigation__item">
						<a href="contacts.html" className="menu__link link-hover">
							Контакти
						</a>
					</li>
				</ul>
			</div>
		</>
	);
};

export default Menu;
