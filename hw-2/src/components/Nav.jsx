import Logo from "./NavComponents/Logo";
import Menu from "./NavComponents/Menu";
import Cart from "./NavComponents/Cart";
import Favorite from "./NavComponents/Favorite";

export default function Nav() {
	return (
		<div className="container sm:px-8 mx-auto px-10 py-6 flex justify-between mb-10 items-center bg-cyan-50 border-b-[1px]">
			<Logo />
			<div className="flex items-center divide-gray-400 divide-x-[1px]">
				<Menu />
				<div className="flex items-center gap-8 pl-6">
					<Favorite />
					<Cart />
				</div>
			</div>
		</div>
	);
}
