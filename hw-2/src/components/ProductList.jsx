import React, { useEffect, useState } from "react";
import ProductCard from "./ProductCard";
import getProducts from "../util/GetProducts";


const ProductList = () => {
	// Задаємо стан завантаженюю товарів
	const [products, setProducts] = useState([]);

	// Завантажуємо товари при першому заході на сайт
	useEffect(() => {

		// Отримуємо товари
		const setProductsList = async () => await getProducts();

		// Записуємо масив товарів з бази даних в стан
		setProductsList().then((data) => {
			setProducts(data);
		});

	}, []);



	return (
		<>
			<div className="container px-10 sm:px-8 mx-auto mb-14 flex flex-wrap gap-6 justify-between">
				{products.map((product) => (
					<ProductCard key={product.id} product={product} />
				))}
			</div>
		</>
	);
};

export default ProductList;
