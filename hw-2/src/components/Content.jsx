import ProductCard from "./ProductCard";

export default function Content() {

	return (
		<div className="catalog mb-10">
			<div className="container mx-auto px-10 sm:px-8">
				<div className="catalog__content">
					<ProductCard />
				</div>
			</div>
		</div>
	);
}
