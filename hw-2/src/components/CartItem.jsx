import React from "react";
import PropTypes from "prop-types";

export default function CartItem({ item }) {
	return (
		<li  className=" cursor-pointer px-2 py-4 flex items-center justify-between hover:bg-gray-100">
			<div className="flex items-center gap-2 max-w-[50%]">
				<div className="h-10 w-10 min-w-[40px] rounded-lg overflow-hidden object-cover">
					<img className="m-0 p-0 w-full h-full object-cover" src={item.img} alt="" />
				</div>
				<div>
					<p>{item.name}</p>
					<div className="h-4 w-4" style={{ background: item.color }}></div>
				</div>
			</div>
			<div className="grid grid-cols-2 min-w-[40%]">
				<span className="text-gray-500">x{item.quantity}</span>
				<span>{(+item.price * +item.quantity).toFixed(2)}</span>
			</div>
		</li>
	);
}

CartItem.propTypes = {
	item: PropTypes.shape({
		name: PropTypes.string,
		price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
		quantity: PropTypes.number,
		img: PropTypes.string,
		color: PropTypes.string
	})
};
