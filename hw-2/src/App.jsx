import { createContext, useState } from "react";
import HomePage from "./pages";
import { getStorage } from "./util";

export const Context = createContext();

function App() {
	const favoriteIds = getStorage("favorite");
	const cart = getStorage("cart");

	const [state, setState] = useState({ favoriteIds, cart });

	return (
		<Context.Provider value={[state, setState]}>
			<HomePage />
		</Context.Provider>
	);
}

export default App;
