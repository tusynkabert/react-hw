import { setStorage } from ".";

// Функція AddToCart для додавання продукту до корзини
const AddToCart = (product, setCart, cart) => {
    // Пошук індексу продукту у корзині
    const productExistKey = cart.findIndex((cartItem) => cartItem.id === product.id);

    // Додавання продукту до корзини, якщо його не було
    if (productExistKey === -1) {
        // Копіюємо поточну корзину та додаємо новий продукт
        const updatedCart = [...cart, product];

        // Оновлення корзини в локальному сховищі
        setStorage('cart', updatedCart);

        // Оновлення стану корзини у батьківському компоненті
        setCart(updatedCart);
    }
};

export default AddToCart;
