import axios from "axios";
import { getStorage, setStorage } from ".";

// Функція getProducts для отримання списку товарів з сервера або локального сховища
const getProducts = async () => {
    // Отримання списку товарів з локального сховища
    let products = getStorage('products');

    try {
        // Перевірка, чи список товарів є порожнім
        if (products.length === 0) {
            // Запит до сервера для отримання списку товарів
            const res = await axios.get("/products.json");
            
            // Отримання та збереження списку товарів
            products = res.data.products;
            setStorage('products', products);
        }

        // Повертаємо список товарів
        return products;

    } catch (error) {
        // Вивід помилки в консоль у випадку невдачі запиту
        console.log(error);
    }
};

// Експорт функції getProducts
export default getProducts;
