// Записуємо дані в сховище
const setStorage = (key, val = false) => (val) ? localStorage.setItem(key, JSON.stringify(val)) : '';

// Дістаємо дані зі сховища
const getStorage = (key) => JSON.parse(localStorage.getItem(key)) ?? [];


export { setStorage, getStorage };