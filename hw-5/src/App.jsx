import HomePage from "./pages";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { CartPage } from "./pages/CartPage";
import { FavoritePage } from "./pages/FavoritePage";
import Layout from "./pages/Layout";
import { Provider } from "react-redux";
import { store } from "./store.js";
import ThankYou from "./pages/ThankYou";

function App() {
	return (
		<Provider store={store}>
			<BrowserRouter>
				<Routes>
					<Route path="/" element={<Layout />}>
						<Route index element={<HomePage />} />
						<Route path={`/cart`} element={<CartPage />} />
						<Route path={`/favorites`} element={<FavoritePage />} />
						<Route path={`/thank-you`} element={<ThankYou />} />
					</Route>
				</Routes>
			</BrowserRouter>
		</Provider>
	);
}

export default App;
