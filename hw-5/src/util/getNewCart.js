import { setStorage } from ".";

// Функція AddToCart для додавання продукту до корзини
const getNewCart = ({product, cart, productId}) => {
    const cartNewItem = product;

    // Створення нового масиву на основі поточного стану корзини
    const updatedCart = [...cart];

    // Перевірка, чи продукт вже існує в корзині
    const productExistKey = updatedCart.findIndex((cartItem) => cartItem.id === cartNewItem.id);

    // Оновлення кількості продукту в корзині
    const newCart = updatedCart.map((cartItem) => {
        if (cartItem.id === productId) {
            return {
                ...cartItem,
                quantity: cartItem.quantity + 1,
            };
        }

        return cartItem;
    });

    // Додавання нового продукту, якщо його не було у корзині
    if (productExistKey === -1) {
        newCart.push({
            ...cartNewItem,
            quantity: 1,
        });
    }

    setStorage("cart", newCart);

    return newCart

};

export default getNewCart;
