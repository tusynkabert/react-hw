import axios from "axios";
// Функція getProducts для отримання списку товарів з сервера або локального сховища
const getProducts = async () => {

    try {

        // Запит до сервера для отримання списку товарів
        const res = await axios.get("/products.json");

        // Отримання та збереження списку товарів
        const products = res.data.products;


        // Повертаємо список товарів
        return products;

    } catch (error) {
        // Вивід помилки в консоль у випадку невдачі запиту
        console.log(error);
    }
};

// Експорт функції getProducts
export default getProducts;
