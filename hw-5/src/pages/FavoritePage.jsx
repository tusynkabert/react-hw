import React from "react";
import ProductCard from "../components/ProductCard";
import { useSelector } from "react-redux";

export const FavoritePage = () => {

    const favoritesList = useSelector((state) => state.favorites.list)
    const products = useSelector((state) => state.products.products)

    // const {favorites : {list: favoritesList},products: {products} } = useSelector((state) => state)

    const selected = products.filter(p => favoritesList.includes(p.id))

    return <div className="container pt-[140px] px-10 sm:px-8 mx-auto mb-14 flex flex-wrap gap-6 justify-between">
                {
                    selected.length === 0
                    ? <h2>No favorite products!</h2>
                    : selected.map((product) => (
                        <ProductCard key={product.id} product={product} />
                    ))}
            </div>
}
 