import React from 'react'

export default function CartEmpty() {
  return (
    <h2>Cart empty!</h2>
  )
}
