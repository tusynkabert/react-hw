import React, { useState } from "react";
import ProductCard from "../ProductCard";
import { Link } from "react-router-dom";
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { PatternFormat } from 'react-number-format';
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { cleanCart } from "../../redux/cartSlice";

const validationSchema = Yup.object({
    firstName: Yup.string().required('First name is required'),
    lastName: Yup.string().required('Last name is required'),
    phoneNumber: Yup.string().required('Phone number is required'),
    age: Yup.number().required('Age is required').min(18, 'Must be at least 18 years old').max(100, 'Must be lower than 100 years old'),
    streetAddress: Yup.string().required('Address is required')
});

export default function CartFilled({ cart }) {
    const navigate = useNavigate();
    const dispatch = useDispatch();
  
    const formik = useFormik({
        initialValues: {
          firstName: '',
          lastName: '',
          phoneNumber: '',
          age: '',
          streetAddress: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            // виведення в консоль інформацію про замовлення та дані користувача та очищуємо корзину
            console.log(values);
            console.log(cart);
            dispatch(cleanCart());
            
            // Оповіщаємо про успішне замовлення
            navigate('/thank-you');
        },
    });

	return (
        <>
			<div className="flex flex-wrap gap-6 justify-between">
				{cart.map((product) => (
                    <ProductCard key={product.id} product={product} needButtonCart={false} />
				))}
			</div>
			<form className="my-20 mx-auto max-w-[600px]" onSubmit={formik.handleSubmit}>
				<div className="border border-gray-200 bg-slate-50 p-12 rounded-xl">
					<h2 className="text-4xl text-gray-900">Оформлення замовлення:</h2>
					<p className="mt-1 text-sm leading-6 text-gray-600">Use a permanent address where you can receive mail.</p>

					<div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
						<div className="sm:col-span-3">
							<label htmlFor="first-name" className="block text-sm font-medium leading-6 text-gray-900">
								First name
							</label>
							<div className="mt-2">
                            <input
                                type="text"
                                name="firstName"
                                id="first-name"
                                maxLength="200"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.firstName}
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 px-3"
                            />
                            {
                            formik.touched.firstName && formik.errors.firstName
                                ? (<div className="text-red-600">{formik.errors.firstName}</div>)
                                : null
                            }
							</div>
						</div>

						<div className="sm:col-span-3">
							<label htmlFor="last-name" className="block text-sm font-medium leading-6 text-gray-900">
								Last name
							</label>
							<div className="mt-2">
                            <input
                                type="text"
                                name="lastName"
                                id="last-name"
                                maxLength="200"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.lastName}
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 px-3"
                            />
                            {formik.touched.lastName && formik.errors.lastName ? (
                                <div className="text-red-600">{formik.errors.lastName}</div>
                            ) : null}
							</div>
						</div>

						<div className="col-span-4">
							<label htmlFor="phone-number" className="block text-sm font-medium leading-6 text-gray-900">
								Phone number
							</label>
							<div className="mt-2">
                            <PatternFormat
                                name="phoneNumber"
                                id="phone-number"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.phoneNumber}
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 px-3"
                                format="+1 (###) ###-##-##"
                                placeholder="+1 (000) 000-00-00"
                            />
                            {formik.touched.phoneNumber && formik.errors.phoneNumber ? (
                                <div className="text-red-600">{formik.errors.phoneNumber}</div>
                            ) : null}
							</div>
						</div>

						<div className="sm:col-span-2">
							<label htmlFor="age" className="block text-sm font-medium leading-6 text-gray-900">
								Your age
							</label>
							<div className="mt-2">
                                <input
                                    type="number"
                                    name="age"
                                    id="age"
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    value={formik.values.age}
                                    className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 px-3"
                                    placeholder="18+"
                                />
                                {formik.touched.age && formik.errors.age ? (
                                    <div className="text-red-600">{formik.errors.age}</div>
                                ) : null}
							</div>
						</div>

						<div className="col-span-full">
							<label htmlFor="street-address" className="block text-sm font-medium leading-6 text-gray-900">
								Address
							</label>
							<div className="mt-2">
                            <input
                                type="text"
                                name="streetAddress"
                                id="street-address"
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                value={formik.values.streetAddress}
                                className="block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 px-3"
                                placeholder="Street address, City, Postal code"
                            />
                            {formik.touched.streetAddress && formik.errors.streetAddress ? (
                                <div className="text-red-600">{formik.errors.streetAddress}</div>
                            ) : null}
							</div>
						</div>
					</div>
					<div className="mt-6 flex items-center justify-end gap-x-6">
						<Link to="/" className="text-sm font-semibold leading-6 text-gray-900">
							Cancel
						</Link>
						<button type="submit" className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600">
							Checkout
						</button>
					</div>
				</div>
			</form>
		</>
	);
}
