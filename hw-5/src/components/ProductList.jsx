import React  from "react";
import ProductCard from "./ProductCard";

import { useSelector } from "react-redux";

const ProductList = () => {
	const productsRedux = useSelector((state ) => state.products.products)
	
	// Витягуємо з контексту масив корзини
	const cart = useSelector((state) => state.cart.list)

	return (
		<>
			<div className="container pt-[140px] px-10 sm:px-8 mx-auto mb-14 flex flex-wrap gap-6 justify-between">
				{productsRedux.map((product) => (
					<ProductCard key={product.id} product={product} isExist={cart.some((item) => item.id === product.id)} />
				))}
			</div>
		</>
	);
};

export default ProductList;
