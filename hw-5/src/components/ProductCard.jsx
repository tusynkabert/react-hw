import { HiOutlineStar, HiStar } from "react-icons/hi";
import addToFavorite from "../util/AddToFavorite";
import {  useEffect, useState } from "react";
import Modal from "./Modal";
import PropTypes from "prop-types";
import {classNames} from '../util/classNames.js' 
import getNewCart from "../util/getNewCart";
import { useDispatch, useSelector } from "react-redux";
import { toggleFavorites } from "../redux/favoritesSlice";
import { addToCart } from "../redux/cartSlice";

export default function ProductCard({ product,showQuantity = false,  needButtonCart = true }) {

	const favoriteIds = useSelector((state) => state.favorites.list)
	const cart = useSelector((state) => state.cart.list)

	const dispatch = useDispatch()

	// Стани для відстеження стану модального вікна та визначення, чи продукт є у списку улюблених
	const [open, setOpen] = useState(false);
	const [isFavorite, setIsFavorite] = useState(false);
	const [showAddedToCart, setShowAddedToCart] = useState(false);

	useEffect(() => {
		setShowAddedToCart(cart.some(item => item.id === product.id))
	}, [cart])
	// Деструктуризація властивостей об'єкта "product"
	const { id, name, price, img, article, color, quantity = null } = product;

	// Стврорюємо ІД таймаута
	let timeOutId;
	

	// Ефект для визначення, чи продукт знаходиться у списку улюблених
	useEffect(() => {
		setIsFavorite(favoriteIds.includes(id));

		// Очищаємо таймаут
		return () => {
			timeOutId && clearTimeout(timeOutId)
		}
	}, [favoriteIds, id]);

	// Функція для додавання продукту до корзини
	const addToCartHandler = (product) => {
		getNewCart({product, cart, productId: id})
		// Оновлення контексту з новим станом корзини
		dispatch(addToCart(product))
		// Ховаємо модальне вікно
		setOpen(false);
	};

	const eventAddToCart = () => {
		addToCartHandler(product);
		setShowAddedToCart(true);

		// Створюємо таймаут
		// timeOutId = setTimeout(() => {
		// 	setShowAddedToCart(false)
		// }, 1000	)
	};

	// Функція для додавання або видалення продукту зі списку улюблених
	const eventSetFavorite = () => {
		// Виклик функції для оновлення улюблених продуктів
		addToFavorite(id);
		dispatch(toggleFavorites(id))
	};


	// Функція для додавання продукту до корзини


	return (
		<>
			<div className=" flex-grow flex flex-col gap-4 relative max-w-[390px]">
				<div onClick={eventSetFavorite} className="w-12 h-12 cursor-pointer rounded-full bg-blue-400 flex items-center justify-center absolute top-5 right-5">
					{isFavorite ? <HiStar className="fill-yellow-300 h-6 w-6" /> : <HiOutlineStar className=" h-6 w-6" />}
				</div>
				<div className="w-full object-cover">
					<img
						loading="lazy"
						style={{
							minWidth: "372px",
							width: "100%",
							height: "293px",
							maxWidth: "100%",
							maxHeight: "100%",
						}}
						className="w-full rounded-2xl"
						src={img}
						alt=""
					/>
				</div>
				<div className="flex gap-2 flex-col justify-between">
					<div className="card-product__title-link">
						{name} - {article}
					</div>
					<span className="card-product__price">{showQuantity ? +price * +quantity : price} грн</span>
					{showQuantity && <span className="card-product__price text-lg text-gray-600">x{quantity} products in cart</span>}
					<div className="flex gap-4 items-center">
						<span
							style={{
								background: color,
							}}
							className="w-4 h-4"
						></span>
						<span>{color}</span>
					</div>
					
					{
						needButtonCart && 
							<button onClick={() => setOpen(true)} className={classNames('w-full cursor-pointer p-4 rounded-lg flex items-center transition-all justify-center text-white', showAddedToCart ? 'bg-green-400' : 'bg-blue-600 ')}>
								<div className="flex items-center gap-2">
									<span>{showAddedToCart ? "Added to cart" : "Add to cart"}</span>
									<div className="card-product__btn-add">
										{!showAddedToCart && <svg className="icon icon-cart">
											<use href="#icon-cart-add"></use>
										</svg>}
									</div>
								</div>
							</button>
					}
				</div>
			</div>
			{open ? <Modal setOpen={setOpen} open={open} eventModal={eventAddToCart} /> : ""}
		</>
	);
}

ProductCard.propTypes = {
	product: PropTypes.shape({
		id: PropTypes.number,
		name: PropTypes.string,
		price: PropTypes.number,
		img: PropTypes.string,
		article: PropTypes.string,
		color: PropTypes.string,
	}),
};
