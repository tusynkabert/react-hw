import React from "react";
import { logoUrl } from "../../images";
import { Link } from "react-router-dom";


const Logo = () => {
	return (
		<>
			<div className="navigation__middle">
				<Link to="/" className="text-2xl flex items-center">
					<img src={logoUrl} className="logo-img max-w-[52px] mr-2" alt="react shop" />
					React Shop
				</Link>
			</div>
		</>
	);
};

export default Logo;
