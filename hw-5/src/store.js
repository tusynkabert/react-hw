import { configureStore } from '@reduxjs/toolkit'
import productsReducer from './redux/productsSlice'
import favoritesReducer from './redux/favoritesSlice'
import cartReducer from './redux/cartSlice'

export const store = configureStore({
  reducer: {
    products: productsReducer,
    favorites: favoritesReducer,
    cart: cartReducer
  },
})
