import { createSlice } from '@reduxjs/toolkit'
import { getStorage } from '../util';

const favoritesSlice = createSlice({
    name: 'favorites',
    initialState: {
        list: getStorage('favorite') || []
    },
    reducers: {
        toggleFavorites(state, action) {
            const id = action.payload;
            if (state.list.indexOf(id) < 0) {
                state.list.push(action.payload)
            }
            else {
                state.list = state.list.filter(fId => fId !== id)
            }
        }
    },
})

export const { toggleFavorites } = favoritesSlice.actions;
export default favoritesSlice.reducer;