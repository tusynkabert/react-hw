import { useState } from "react";
import "./App.scss";
import Button from "./components/Button";
import Modal from "./components/Modal";

function App() {
  const [open, setOpen] = useState(false);
  const [modalId, setModalId] = useState(null);

  const modals = [
    {
      id: 1,
      header: "Do you want to delete this file?",
      closeButton: true,
      text: `Once you delete this file, it won’t be possible to undo this action.  Are you sure you want to delete it?`,
      actions: (
        <div className="modal__actions">
          <Button
            onClick={() => setOpen(false)}
            text={"Ok"}
            className="btn btn-ok"
          />
          <Button
            onClick={() => setOpen(false)}
            text={"Cancel"}
            className="btn btn-cancel"
          />
        </div>
      ),
    },
    {
      id: 2,
      header: "Do you want to exit this form?",
      closeButton: true,
      text: `After exiting this file, the changes will not be saved. Are you sure you want to exit and not save your changes?`,
      actions: (
        <div className="modal__actions">
          <Button
            onClick={() => setOpen(false)}
            text={"Exit"}
            className="btn btn-exit"
          />
          <Button
            onClick={() => setOpen(false)}
            text={"Save changes and exit"}
            className="btn btn-save"
          />
        </div>
      ),
    },
  ];

  const getCurrentModal = () => {
    const modal = modals.find((modal) => modal.id === modalId);
    return (
      <Modal
        setOpen={setOpen}
        header={modal.header}
        closeButton={modal.closeButton}
        text={modal.text}
        actions={modal.actions}
      />
    );
  };

  return (
    <div className="App">
      <Button
        text="Open first modal"
        onClick={() => {
          setModalId(1);
          setOpen(true);
        }}
        className="btn btn-first-modal"
      />
      <Button
        text="Open second modal"
        onClick={() => {
          setModalId(2);
          setOpen(true);
        }}
        className="btn btn-second-modal"
      />
      {open && modalId ? getCurrentModal() : null}
    </div>
  );
}

export default App;

// Fira Code
