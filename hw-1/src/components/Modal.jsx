import React from "react";
import Button from "./Button";

const Modal = ({ header, closeButton = true, text, actions, setOpen }) => {
  return (
    <div className="modal">
      <div onClick={() => setOpen(false)} className="modal__overlay"></div>
      <div className="modal__body">
        {closeButton && (
          <Button
            className={"modal__close"}
            text="X"
            onClick={() => setOpen(false)}
          />
        )}
        <h2 className="modal__header">{header}</h2>
        <p className="modal__text">{text}</p>
        {actions}
      </div>
    </div>
  );
};

export default Modal;
